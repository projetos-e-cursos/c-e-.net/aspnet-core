﻿using Alura.ListaLeitura.App.HTML;
using Alura.ListaLeitura.App.Negocio;
using Alura.ListaLeitura.App.Repositorio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alura.ListaLeitura.App.Logica
{
    public class LivrosController :Controller
    {
        public IEnumerable<Livro> Livros { get; set; }
        public string Detalhes(int id)
        {
            var repo = new LivroRepositorioCSV();
            var livro = repo.Todos.First(l => l.Id == id);
            return livro.Detalhes();
        }

        private static string CarregaLista(IEnumerable<Livro> livros, string titulo)
        {
            var conteudoArquivo = HtmlUtils.CarregaArquivoHTML("lista");
            
            return conteudoArquivo
                .Replace("#TITULO#", titulo)
                .Replace("#NOVO-ITEM#", "");
        }

        public IActionResult ParaLer()
        {
            var _repo = new LivroRepositorioCSV();
            //var html = CarregaLista(_repo.ParaLer.Livros, "Livros Para Ler:");
            ViewBag.Livros = _repo.ParaLer.Livros;
            ViewBag.Titulo = "Livros Para Ler:";
            var html = new ViewResult { ViewName = "" };
            return View("lista_itens");
        }

        public IActionResult Lendo()
        {
            var _repo = new LivroRepositorioCSV();
            // var html = CarregaLista(_repo.Lendo.Livros, "Livros Lendo:");
            ViewBag.Livros = _repo.Lendo.Livros;
            ViewBag.Titulo = "Livros Lendo:";
            return View("lista_itens"); ;
        }

        public IActionResult Lidos()
        {
            var _repo = new LivroRepositorioCSV();
            // var html = CarregaLista(_repo.Lidos.Livros, "Livros Lidos:");
            ViewBag.Livros = _repo.Lidos.Livros;
            ViewBag.Titulo = "Livros Lidos:";
            return View("lista_itens");
        }

        public string Teste()
        {
            return "Nova funcionalidade funciona";
        }
    }
}
